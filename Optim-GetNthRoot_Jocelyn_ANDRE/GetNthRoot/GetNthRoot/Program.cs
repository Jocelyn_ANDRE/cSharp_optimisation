﻿using GetNthRoot.MathOperations;

Console.WriteLine("Entrer le nombre à atteindre : ");

double number = 0;
var root = 0;

var isCorrectlyEntered = false;
while (!isCorrectlyEntered)
{
    try
    {
        number = Convert.ToDouble(Console.ReadLine());
        isCorrectlyEntered = true;
    }
    catch (FormatException)
    {
        Console.WriteLine("Entered value is not in the correct format.");
    }
}

Console.WriteLine("Entrer la puissance : ");

isCorrectlyEntered = false;
while (!isCorrectlyEntered)
{
    try
    {
        root = Convert.ToInt32(Console.ReadLine());
        isCorrectlyEntered = true;
    }
    catch (FormatException)
    {
        Console.WriteLine("Entered value is not in the correct format.");
    }
}

var nthRoot = ComplexMathOperation.GetNthRoot(number, root);

Console.WriteLine("The nth root (rounded to 0.0001) of {0} by {1} is {2}", number, root, nthRoot.ToString("0.####"));



