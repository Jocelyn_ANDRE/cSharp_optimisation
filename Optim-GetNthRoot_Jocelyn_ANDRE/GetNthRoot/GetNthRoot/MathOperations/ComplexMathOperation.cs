namespace GetNthRoot.MathOperations;

internal static class ComplexMathOperation
{
    private const double Rounded = 0.0000000000001;
    public static double GetNthRoot(double searchedNumber, int n)
    {
        double min = 0;
        var max = double.MaxValue;
        double middle = 0;
        var found = false;

        while (!found)
        {
            middle = GetMiddle(min, max);
            var result = GetPower(middle, n);

            if (IsClosed(min, max))
            {
                return middle;
            }

            if (result > searchedNumber)
            {
                max = middle;
                continue;
            }

            if (result < searchedNumber)
            {
                min = middle;
                continue;
            }

            found = true;
        }
        
        return middle;
    }

    private static double GetPower(double value, int n)
    {
        double result = 1;

        for (var i = 0; i < n; i++)
        {
            result *= value;
        }

        return result;
    }

    private static double GetMiddle(double min, double max) => (min + max) / 2;

    private static bool IsClosed(double min, double max) => (max - min) < Rounded;
}