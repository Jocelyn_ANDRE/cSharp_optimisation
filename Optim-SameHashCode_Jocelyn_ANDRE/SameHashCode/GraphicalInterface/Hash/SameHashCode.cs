using System;
using System.Collections.Generic;
using System.Text;

namespace GraphicalInterface.Hash;

public class SameHashCode
{
    private const string PossiblesChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public (int hash, string string1, string string2, string string3) GenerateSameHashCode()
    {
        var possiblesCharCount = PossiblesChar.Length;

        var stringWithHash = new Dictionary<int, string>();
        var stringWithHashDuplicate = new Dictionary<int, string>();
        (int hash, string value) lastStringWithHash = (0, string.Empty);

        var random = new Random();
        var stringBuilder = new StringBuilder();

        var found = false;
        while (!found)
        {
            var stringLength = random.Next(1, 100);
            stringBuilder.Clear();
            
            for (var i = 0; i < stringLength; i++)
            {
                stringBuilder.Append(PossiblesChar[random.Next(0, possiblesCharCount)]); 
            }

            var hashCode = stringBuilder.ToString().GetHashCode();

            if (stringWithHash.TryAdd(hashCode, stringBuilder.ToString()))
            {
                continue;
            }

            if (stringBuilder.ToString() == stringWithHash.GetValueOrDefault(hashCode))
            {
                continue;
            }

            if (stringWithHashDuplicate.TryAdd(hashCode, stringBuilder.ToString()))
            {
                continue;
            }
            
            if (stringBuilder.ToString() == stringWithHashDuplicate.GetValueOrDefault(hashCode))
            {
                continue;
            }

            lastStringWithHash = (hashCode, stringBuilder.ToString());
            
            found = true;
        }

        var hash = lastStringWithHash.hash;
        var stringOne = stringWithHash.GetValueOrDefault(hash);
        var stringTwo = stringWithHashDuplicate.GetValueOrDefault(hash);
        var stringThree = lastStringWithHash.value;

        return (hash, stringOne, stringTwo, stringThree);
    }
}