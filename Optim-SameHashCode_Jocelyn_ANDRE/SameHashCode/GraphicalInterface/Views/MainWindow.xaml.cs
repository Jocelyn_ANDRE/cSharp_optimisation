﻿using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using GraphicalInterface.Hash;
using GraphicalInterface.ViewModels;

namespace GraphicalInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly HashCodeViewModel viewModel;
        
        public MainWindow()
        {
            InitializeComponent();
            viewModel = new HashCodeViewModel();
            DataContext = viewModel;
            Application.Current.MainWindow.Closing += new CancelEventHandler(MainWindow_Closing);
        }

        private void MainWindow_Closing(object? sender, CancelEventArgs e)
        {
        }
    }
}