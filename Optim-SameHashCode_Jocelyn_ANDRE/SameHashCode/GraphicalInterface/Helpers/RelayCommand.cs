using System;
using System.Windows.Input;

namespace GraphicalInterface.Helpers;

public class RelayCommand : ICommand
{
    private readonly Func<bool> _canExecute;
    private readonly Action<object> _methodToExecute;


    public RelayCommand(Action<object> methodToExecute, Func<bool> canExecute = null)
    {
        _methodToExecute = methodToExecute;
        _canExecute = canExecute;
    }

    public RelayCommand(Action methodToExecute, Func<bool> canExecute = null)
    {
        _methodToExecute = _ => methodToExecute();
        _canExecute = canExecute;
    }

    public bool CanExecute(object parameter)
    {
        if (_canExecute is null)
            return true;

        var result = _canExecute.Invoke();
        return result;

    }

    public void Execute(object parameter)
    {
        if (_methodToExecute is not null)
            _methodToExecute.Invoke(parameter);
    }

    public event EventHandler CanExecuteChanged
    {
        add { CommandManager.RequerySuggested += value; }
        remove { CommandManager.RequerySuggested -= value; }
    }
}