using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using GraphicalInterface.Hash;
using GraphicalInterface.Helpers;

namespace GraphicalInterface.ViewModels;

public class HashCodeViewModel : INotifyPropertyChanged
{
    public ICommand GenerateSameHashCodes { get; set; }

    private string _hashCode = string.Empty;
    public string HashCode { get => _hashCode; set { _hashCode = value; OnPropertyChanged(nameof(HashCode)); } }
    
    private string _string1 = string.Empty;
    public string String1 { get => _string1; set { _string1 = value; OnPropertyChanged(nameof(String1)); } }
    
    private string _string2 = string.Empty;
    public string String2 { get => _string2; set { _string2 = value; OnPropertyChanged(nameof(String2)); } }
    
    private string _string3 = string.Empty;
    public string String3 { get => _string3; set { _string3 = value; OnPropertyChanged(nameof(String3)); } }

    private bool _canLaunchGeneration = true;
    public bool CanLaunchGeneration { get => _canLaunchGeneration; set { _canLaunchGeneration = value; OnPropertyChanged(nameof(CanLaunchGeneration)); } }
    
    public HashCodeViewModel()
    {
        GenerateSameHashCodes = new RelayCommand(GenerateAndDisplayHashCode);
    }
    
    private void GenerateAndDisplayHashCode()
    {
        //ThreadPool.QueueUserWorkItem(GenerateAndDisplayHashCode);
        CanLaunchGeneration = false;
        Task.Run(GenerateAndDisplayHashCodeTask);
    }

    private void GenerateAndDisplayHashCodeTask()
    {
        var sameHashCode = new SameHashCode();
        var threeStringsWithSameHash = sameHashCode.GenerateSameHashCode();

        HashCode = threeStringsWithSameHash.hash.ToString();
        String1 = threeStringsWithSameHash.string1;
        String2 = threeStringsWithSameHash.string2;
        String3 = threeStringsWithSameHash.string3;
        CanLaunchGeneration = true;
    }
    
    public event PropertyChangedEventHandler? PropertyChanged;
    protected void OnPropertyChanged(string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
}