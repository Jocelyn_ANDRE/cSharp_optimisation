﻿using System.Text;

//Variables utilisées pour la sélection d'un caractère aléatoire
const string possiblesChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
var possiblesCharCount = possiblesChar.Length;

//Variables utilisées pour stocker les string alétoires créées et leur hash correspondant
var stringWithHash = new Dictionary<int, string>();
var stringWithHashDuplicate = new Dictionary<int, string>();
var stringWithHashThird = new Dictionary<int, string>();
var stringWithHashFourth = new Dictionary<int, string>();
(int hash, string value) lastStringWithHash = (0, string.Empty);

var random = new Random();
var stringBuilder = new StringBuilder();
var iteration = 0;

var found = false;
while (!found)
{
    var stringLength = random.Next(1, 100);
    stringBuilder.Clear();
    iteration++;
    
    for (var i = 0; i < stringLength; i++)
    {
        stringBuilder.Append(possiblesChar[random.Next(0, possiblesCharCount)]); 
    }

    var hachCode = stringBuilder.ToString().GetHashCode();

    if (stringWithHash.TryAdd(hachCode, stringBuilder.ToString()))
    {
        continue;
    }

    if (stringBuilder.ToString() == stringWithHash.GetValueOrDefault(hachCode))
    {
        continue;
    }

    if (stringWithHashDuplicate.TryAdd(hachCode, stringBuilder.ToString()))
    {
        continue;
    }
    
    if (stringBuilder.ToString() == stringWithHashDuplicate.GetValueOrDefault(hachCode))
    {
        continue;
    }
    
    if (stringWithHashThird.TryAdd(hachCode, stringBuilder.ToString()))
    {
        continue;
    }
    
    if (stringBuilder.ToString() == stringWithHashThird.GetValueOrDefault(hachCode))
    {
        continue;
    }
    
    if (stringWithHashFourth.TryAdd(hachCode, stringBuilder.ToString()))
    {
        continue;
    }
    
    if (stringBuilder.ToString() == stringWithHashFourth.GetValueOrDefault(hachCode))
    {
        continue;
    }

    lastStringWithHash =  (hachCode, stringBuilder.ToString());
    
    found = true;
}

var stringOne = stringWithHash.GetValueOrDefault(lastStringWithHash.hash);
var stringTwo = stringWithHashDuplicate.GetValueOrDefault(lastStringWithHash.hash);
var stringThree = stringWithHashThird.GetValueOrDefault(lastStringWithHash.hash);
var stringFour = stringWithHashFourth.GetValueOrDefault(lastStringWithHash.hash);

Console.WriteLine(iteration);

Console.WriteLine("Found duplicate hash {0} for strings\n{1}\n{2}\n{3}\n{4}\n{5}",
                    lastStringWithHash.hash, stringOne, stringTwo, stringThree, stringFour, lastStringWithHash.value);